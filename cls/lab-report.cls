\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{lab-report}[2021/03/18 nuaNce's custom lab report class]
\LoadClass[10pt]{article}

%font
\RequirePackage{libertine}
\RequirePackage{libertinust1math}
\RequirePackage[T1]{fontenc}
\RequirePackage{microtype}

%maths and shit
\RequirePackage{mathtools}
\RequirePackage[version=4]{mhchem}
\RequirePackage{chemfig}
\RequirePackage{booktabs}
\RequirePackage{changepage}
\RequirePackage{multicol}
\RequirePackage{graphicx}
\RequirePackage{siunitx}
\RequirePackage{setspace}
\RequirePackage{blindtext}
\RequirePackage{enumitem}

%sections
\RequirePackage{sectsty}
\setcounter{secnumdepth}{0}
\sectionfont{\large\bfseries}
\subsectionfont{\normalsize\bfseries}
\RequirePackage[compact]{titlesec}

%bibliography
\RequirePackage[breaklinks,
	colorlinks = true,
	linkcolor = black,
	urlcolor  = black,
	citecolor = black,
	anchorcolor = black]{hyperref}
\usepackage[style=apa,backend=biber]{biblatex}
\addbibresource{bibliography.bib}

%page styling
\RequirePackage[letterpaper,top=30mm,bottom=40mm,left=50mm,right=50mm]{geometry}
\renewcommand{\baselinestretch}{1.1}
\RequirePackage{parskip}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.4pt}
\lfoot{\textbf{Lab Report:} Boyle's Law}
\rfoot{\thepage}
\cfoot{}
\lhead{}
\rhead{}

%custom commands
\RequirePackage{xcolor}
\newcommand{\hl}{\colorbox{blue!20}}
\newcommand{\red}{\textcolor{red!70!black}}
\newcommand{\blue}{\textcolor{cyan!70!black}}
\newcommand{\green}{\textcolor{green!70!black}}
\newcommand{\purple}{\textcolor{magenta!70!black}}
\newcommand{\br}{%
	\columnbreak{}
}
\newenvironment{answer}{\vspace{2mm}\begin{adjustwidth}{5mm}{}}{\end{adjustwidth}\vspace{2mm}}

% best command
\raggedright{}
