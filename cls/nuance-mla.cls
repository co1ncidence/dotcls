\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{nuance-mla}[2021/03/27 nuaNce's custom MLA class]
\LoadClass[10pt, letterpaper]{article}

%typesetting
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}

%typography and page
\RequirePackage{mathptmx}
\RequirePackage{parskip}
\RequirePackage[letterpaper]{geometry}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
\RequirePackage{csquotes}

%bibliography
\RequirePackage[breaklinks,
	colorlinks = true,
	linkcolor = black,
	urlcolor  = blue,
	citecolor = black,
	anchorcolor = black]{hyperref}
\RequirePackage[style=mla-new, backend=biber]{biblatex}
\addbibresource{bibliography.bib}

%spacing
\RequirePackage{setspace}
\doublespacing{}

%header
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\lhead{}
\chead{}
\rhead{Chaudhry \thepage}
\lfoot{}
\cfoot{}
\rfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\endinput
