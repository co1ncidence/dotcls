\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{cheatsheet}[2021/03/18 nuaNce's custom cheatsheet class]
\LoadClass[10pt]{article}

%typesetting
\RequirePackage[english]{babel}
\RequirePackage[T1]{fontenc}

%typography
\RequirePackage[p]{cochineal}
\RequirePackage[cochineal,bigdelims,cmintegrals,vvarbb]{newtxmath}
\RequirePackage[zerostyle=c,scaled=.94]{newtxtt}
\RequirePackage[cal=boondoxo]{mathalfa}
\RequirePackage[scale=.95,type1]{cabin}

%section styling
\RequirePackage{sectsty}
\setcounter{secnumdepth}{0}
\sectionfont{\normalsize\sffamily\bfseries}
\subsectionfont{\normalsize\sffamily\bfseries}
\RequirePackage[compact]{titlesec}

%text settings
\renewcommand{\baselinestretch}{1}
\renewcommand{\familydefault}{\rmdefault}
\RequirePackage{microtype}

%other necessities
\RequirePackage{xcolor}
\RequirePackage{enumitem}
\RequirePackage{tabularx}
\RequirePackage{gensymb}
\RequirePackage{blindtext}
\RequirePackage[makeroom]{cancel}
\RequirePackage{siunitx}

%page layout
\RequirePackage[letterpaper, landscape, margin=0.2in]{geometry}
\RequirePackage{changepage}
\RequirePackage{multicol}
\setlength{\columnsep}{5mm}
\RequirePackage{ragged2e}
\RequirePackage[compact]{titlesec}
\RequirePackage{fancyhdr}
\RequirePackage{parskip}
\pagestyle{empty}
\lhead{}
\rhead{}
\lfoot{}
\rfoot{}
\cfoot{}

%usability packages
\RequirePackage[most]{tcolorbox}
\RequirePackage[version=4]{mhchem}
\RequirePackage{stackengine}
\RequirePackage{mathtools}
\RequirePackage{booktabs}
\RequirePackage{chemfig}
\RequirePackage{tikz}

%images
\RequirePackage[font=small,labelfont=bf]{caption}
\RequirePackage{graphicx,ifthen}
\graphicspath{{./images/} }

%urls
\RequirePackage{url}
\RequirePackage{breakurl}
\def\UrlBreaks{\do\/\do-}
\RequirePackage[breaklinks,
	colorlinks = true,
	linkcolor = black,
	urlcolor  = black,
	citecolor = black,
	anchorcolor = black]{hyperref}
\hypersetup{colorlinks=true, linkcolor=black, filecolor=magenta, urlcolor=blue,}
\urlstyle{same}

%custom commands
\newcommand{\hl}{\colorbox{blue!20}}
\newcommand{\red}{\textcolor{red!70!black}}
\newcommand{\blue}{\textcolor{cyan!70!black}}
\newcommand{\green}{\textcolor{green!70!black}}
\newcommand{\purple}{\textcolor{magenta!70!black}}
\newcommand{\br}{%
	\columnbreak{}
}

%code for "callout" tcolorbox
\tcbset{%
	lemmastyle/.style={enhanced, colback=blue!5, colframe=blue!20, arc=0pt,
			fonttitle=\bfseries, description color=blue!60!black,
			colbacktitle=blue!5, coltitle=black!50!black,
			top=\tcboxedtitleheight,
			boxed title style={arc=0pt},
			attach boxed title to top left={yshift=-\tcboxedtitleheight/2,
					xshift=4mm}%
		},
}
\newtcbtheorem{callout}{Note}{lemmastyle}{thm}

%shoutout env
\newenvironment{shoutout}
{\begin{Center}}
		{\end{Center}}

%remove list indents
\setlist[itemize]{leftmargin=1.2em}
\setlist[enumerate]{leftmargin=1.2em}

%looks cleaner imo
\raggedcolumns{}

%fuck
\raggedright{}

% bruh
\newenvironment{answer}{\begin{adjustwidth}{2mm}{}}{\end{adjustwidth}}

\endinput
